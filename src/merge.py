import os;
class csvmerge:
	def __init__(self,fileA,fileB,mainIndex,refIndex):
		self.csvA = fileA
		self.csvB = fileB
		self.CNAME_LOC = mainIndex #contact column location
		self.STATE_LOC = refIndex #another reference column location
		try:
			with open(self.csvA) as f:
				csvcontent = f.readlines()
		except:
			print "Error reading from csv A : " + self.csvA
		self.start(csvcontent)
		
	def arr_length(self,arr):
	    _len = 0
		for e in arr:
			_len = _len + len(e)
		return e
		
   def start(self,csvcontent):     
	    redu = 0
		marker1 = 0
		marker2 = 0
		rawcontent = []
		already_found = []
		csvcontent2 = csvcontent
		for l in csvcontent:
		    flag = 0
			marker1 = marker1 + 1
			csvarray = l.split(",")
			fullname = csvarray[self.CNAME_LOC].split(" ")
			fname = fullname[0]
			lname = fullname[-1]
			marker2 = marker1
			for l2 in csvcontent2[marker1:]:
				marker2 = marker2 + 1
				c = l2.split(",")[self.CNAME_LOC]
				if fname in c and lname in c and len(fname) > 1 and len(lname) > 1 and fname!=lname:
					if len(c.split(" ")[0]) == len(fname) and len(c.split(" ")[-1]) == len(lname):
						if l.split(",")[self.STATE_LOC] == l2.split(",")[self.STATE_LOC]:
							if marker1 != marker2:
								if [marker1,marker2] not in already_found:
									already_found.append([marker1,marker2])
									already_found.append([marker2,marker1])
									flag = 1
									#print l
			if flag == 0:
				rawcontent.append(l)
			self.writemerged(rawcontent)
			
	def writemerged(self,content):                                                                   
		try:
			file = open(self.csvB,"w")
			file.writelines(content)
			file.close()
		except:
			print "Error writing to file : " + self.csvB

csvMerge('old_list.csv','new_list.csv',2,10)