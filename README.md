# CSVMerge

## About
CSVMerge is a simple python script written because the csv module would not work they want it was supposed to work (or understood to work). It essentially searches through a CSV file and finds redundant data (in this case, a CSV file with contact information). 

## Features
Searches for a contact with the assumption that you follow the ACT (CRM system from Sage) way of doing this - a single column for Contact name (instead of a First Name and Last Name column). 

It splits up the contact name with spaces and runs searches looking for commonalities and then compares it to a reference (in this case, a state name). If the spit names and the state matches, it is checked further using string lengths. The method was done with tedious trial and errors and this was the most accurate way of doing things.

## Why The Odd Splitting?

It is meant to work with large amounts of data. I didn't want it splitting strings unless absolutely necessary which is why I have a a specific area in the flow where the line is split. 

## How Can I Modify This?

You can change this to work the way you want by editing the start() method.

## Can I Use Different CSV files?

Originally it used to do that but then we just made it all into one file. Just copy and paste the different files into one. 

## Contraints

The number of columns has to be consistent because we specifiy the indexes.

## How Can I Use This?

Pretty simple. Just rewrite the Class initialization method.

csvmerge([old csv file path],[new file to be written into],[INDEX of the contact column],[INDEX of the reference column])
